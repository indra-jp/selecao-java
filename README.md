# Seleção Java

Olá, tudo tranquilo? Se você chegou até aqui, significa que tem interesse em uma das nossas vagas, certo? Basicamente, o seu código será analisado por um dos nossos arquitetos e se você se sair bem, é bem provável que faça parte do nosso time. Desejamos que você seja muito bem sucedido na atividade quem vem a seguir.

## Vagas

Estamos em constante ascenção e temos vagas para todos os perfis.

## Do que se trata a atividade?

Precisamos que você nos mostre que sabe trabalhar com a stack Spring e Angular. Basicamente a ideia consiste na criação de dois projetos, uma API escrita com spring-boot 2.x e banco h2(Relacional) ou MongoDB(Não-Relacional), além de uma camada Web escrita com Angular 8 usando webpack e maven para empacotar e unir os projetos.

Caso você não manje de Angular, encorajamos que você nos envie a API. Claro, quem enviar a API (backend) + Angular (frontend) estará na frente. Justo não?

### O que será avaliado?

* Todos os requistos devem ser atendidos
* Uso da stack Spring
* Uso do padrão arquitetural MVC (no backend e no frontend)
* Padrões de projeto e SOLID
* Coesão
* Baixo acoplamento
* Polimorfismo
* Uso dos plugins do maven
* Documentação interativa da API (Opcional. Ganhará pontos se tiver)
* Evitar código extremamente verboso
* Iremos fazer chamadas direta a sua API sem o front também. Precisamos das requisições HTTP já montadas para todos os recursos

### Requisitos que devem ser implementados

* Utilizando o arquivo MiniTeste.csv presente neste projeto:
* Implemente uma documentação interativa. O acesso a essa URI não requer autenticação
* Implementar recurso para CRUD de Pilotos
* Implementar recurso para CRUD de GP
* Implementar recurso para CRUD de Resultados
* Implementar recurso para importação do csv 'MiniTeste.csv'
* Implementar recurso que retorne o maior vencedor por GP
* Implementar recurso que retorne o maior vencedor por Ano
* Implementar recurso que retorne a média de Rating da corrida por Ano
* Implementar recurso que retorne a melhor corrida da história. Baseada no Rating
* Implementar recurso que retorne a pior corrida da história. Baseada no Rating
* Implementar recurso que retorne todo o histórico de pódio por piloto por ano
* Implementar recurso que retorne todo o histórico de pódio por piloto em todos os anos
* Implementar recurso que retorne a quantidade de GP's que um determinado piloto esteve no podio em sua carreira
* Implementar testes unitários
* OBS: Você deve deduzir quais são as entidades do domínio necessárias para completar a atividade, tal como os relacionamentos, etc

### Não entregue a atividade sem isso

* Uso do framework Lombok. Afinal, ninguém gosta de classes poluídas


### Uma vez feito, como entrego o projeto?

* Crie um fork dessa avaliação
* Desenvolva
* Envie e-mail para rvianal@indracompany.com sinalizando a entrega
* Você possui 3 dias para entregar a atividade. Que comecem os jogos :-)


### O que me desclassificaria automaticamente?

* Commitar sem seguir as regras dos tópicos anteriores
* Sua aplicação não subir
* Não entregar a atividade dentro do prazo estabelecido
